import React, { Component } from 'react';
import './App.css';

const APP_ID = '49f82c9128778b3701a60a3296d5115d';

const BASE_URL = "http://api.openweathermap.org/data/2.5/weather";

class App extends Component {
  state = {
    selectedValue: 'name',
    temp: '',
    cityName: '',
    description: '',
    cityFound: '',
    apiHit: '',
    pastSearches: []
  }

  timeout = null;

  //On input change
  onChangeHandler = (e) => {
    let dropdownValue = this.state.selectedValue;
    let url = BASE_URL + "?appid=" + APP_ID + "&units=metric&";
    switch (dropdownValue) {
      case 'name':
        url += "q=" + e.target.value;
        break;
      case 'lat_long':
        let splitArray = e.target.value.split(",");
        url += "lat=" + splitArray[0] + '&lon=' + splitArray[1];
        break;
      case 'zipcode':
        url += "q=" + e.target.value;
        break;
      default:
        break;
    }
    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      fetch(url)
        .then(res => res.json())
        .then(
          (result) => {
            this.state.cityFound = false;
            this.setState({ apiHit: true });
            if (result.cod === 200) {
              this.setState({ temp: result.main.temp, description: result.weather[0].description, cityName: result.name });
              this.setLocalStorageData(result);
            } else {
              this.setState({ cityFound: true });
            }
          },
          (error) => {

          }
        )
    }, 2000);
  }

  //Set local storage data
  setLocalStorageData = (result) => {
    let pastSearchesArray = {};
    let flag = 1;
    pastSearchesArray['cityName'] = result.name;
    pastSearchesArray['temp'] = result.main.temp;
    let localStorageData = JSON.parse(localStorage.getItem('pstd'));

    //First Item in local storage
    if (localStorageData == null) {
      localStorageData = [];
    } else {
      let i = localStorageData.length;
      //Check if city name is already there in local storage
      while (i--) {
        if ((localStorageData[i]['cityName'] === result.name)) {
          localStorageData[i].temp = result.main.temp;
          flag = 0;
        }
      }
      //Check if array length is greater than 2
      if (flag && localStorageData.length > 2)
        localStorageData.shift();
    }
    //Push item in localStorage
    if (flag) {
      localStorageData.push(pastSearchesArray);
      localStorage.setItem('pstd', JSON.stringify(localStorageData));
    }
    this.setState({ pastSearches: localStorageData });
  }

  //Invoked after component inserted into the tree)
  componentDidMount() {
    let localStorageData = JSON.parse(localStorage.getItem('pstd'));
    this.setState({ pastSearches: localStorageData });
  }

  //Create pase search block
  createBlock = (data) => {
    if (data) {
      let d = data.map(item => {
        return (
          <div className='single-block'>
            <p>City: {item.cityName}</p>
            <p>Status: {Math.floor(item.temp)}&#8451;</p>
          </div>
        )
      })
      return d;
    }
  }

  //On dropdown change
  handleSelectChange = (e) => {
    this.setState({ selectedValue: e.target.value });
  }

  //Render the page
  render() {
    return (
      <div className='container'>
        <div className='input-field-container'>
          <input className='input-field' onChange={this.onChangeHandler} type='input' />
          <select className='dropdown-list' onChange={this.handleSelectChange} value={this.state.selectedValue}>
            <option value="name">Name</option>
            <option value="lat_long">Lon-Lat</option>
            <option value="zipcode">Zip-Code</option>
          </select>

          {(this.state.cityFound) ?
            <div className='city-not-found'>City Not Found</div> :
            (this.state.apiHit) ? <div className='display-weather-container'>
              <p> <span style={{ color: 'blue' }}>Weather Info : </span>{this.state.cityName} {Math.floor(this.state.temp)}&#8451; ({this.state.description})</p>
            </div> : ''
          }

          <div className='past-searches-container'>
            <p id='id12'> Past searches </p>
            {this.createBlock(this.state.pastSearches)}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
